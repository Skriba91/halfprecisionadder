`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/19/2018 10:22:44 AM
// Design Name: 
// Module Name: put_z
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module put_z(

    input clk,rst,
    
    //Handshake jelek
    input align_ready,
    input add_ready,
    input carry_ready,
    input normalise_ready,
    input round_ready,
    input round_vld,
    input special_out_vld,
    input special_vld,
    input out_ack,
    
    //Bejev� �rt�k
    input [9:0] in_z_m,
    input [4:0] in_z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_z_s,          //El�jel bitek
    input [15:0] in_z,
    
    //Kimeneti handshake
    output reg put_vld,         //Az adat k�sz tov�bbi feldolgoz�sra
    output reg put_ack,         //Jelz�s, hogy adatot olvasott be
    output reg put_special_ack,
    
    //Kimenete adat regiszter
    output reg [15:0] z  

    );
    
    reg put_ready;
    
always @ (posedge clk) begin
    //Reset ut�n k�sz adatot fogadni, �s a kimenet �rv�nytelen
    if(rst) begin
        put_ready <= 1;
        put_vld <= 0;
        put_ack <= 0;
        put_special_ack <= 0;
    end
    //Ha k�sz adatot fogadni, �s az el�z� fokozat kimenet�n �rv�nyes az adat
    else if(put_ready && round_vld) begin
        z[9:0] <= in_z_m;
        z[14:10] <= in_z_e[4:0] + 15;
        z[15] <= in_z_s;
        //Ha t�lcsordul�s miatt az exponens nagyobb, mint 15,
        //akkor v�gtelent ad a kimeneten 
        if ($signed(in_z_e) > 15) begin
            z[9 : 0] <= 0;
            z[14 : 10] <= 31;
            z[15] <= in_z_s;
        end
        put_ready <= 0; //Nem tud �j adatot fogadni
        put_ack <= 1;   //Jelez, hogy beolvasta az adatot
        put_vld <= 1;   //�rv�nyes a kimeneten az adat
    end
    //Ha a kimeneten �rv�nyes az adat, akkor �jra tud fogadni
    else if(out_ack) begin
        put_ready <= 1;
        put_vld   <= 0;
    end
    //Ha egy speci�ls sz�m az eredm�ny, �s egyik el�z� fokozat sem dolgozik, akkor
    //�thidalva a k�ztes fokozatokat az erdm�ny egyb�l mehet a kimenetre
    else if(put_ready && special_out_vld && align_ready && add_ready && carry_ready && normalise_ready && round_ready) begin
        z <= in_z;      //K�zvetlen a kimenetbe t�lti a k�sz sz�mot
        put_ready <= 0; //Nem tud �j adatot fogadni
        put_special_ack <= 1;   //Jelez, hogy beolvasta az adatot
        put_vld <= 1;   //�rv�nyes a kimeneten az adat
    end
    //A handshake 1 �rajel hossz�s�g� lehet
    if(put_ack | put_special_ack) begin
        put_ack <= 0;
        put_special_ack <= 0;
    end
end

endmodule
