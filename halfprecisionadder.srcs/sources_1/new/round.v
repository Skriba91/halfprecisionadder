`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/19/2018 10:33:43 AM
// Design Name: 
// Module Name: round
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module round(

    input clk,rst,
    
    input normalise_vld,
    input put_ack,
    
    input [10:0] in_z_m,
    input [4:0] in_z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_z_s,          //El�jel bitek
    input in_round_bit, in_guard, in_sticky,
    
    output reg round_vld,         //Az adat k�sz tov�bbi feldolgoz�sra
    output reg round_ack,         //Jelz�s, hogy adatot olvasott be
    output reg round_ready,       //K�sz adatot fogadni
    
    output reg [9:0] z_m,       //Az eredm�ny mantissz�ja rejtett bittel
    output reg [4:0] z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg z_s          //El�jel bittel
    );

reg round_bit, guard, sticky;
    
always @ (posedge clk) begin
    //Reset ut�n k�sz adatot fogadni, �s a kiement �rv�nytelen
    if(rst) begin
        round_ready <= 1;
        round_vld <= 0;
        round_ack <= 0;
    end
    //Ha k�sz adatot fogadni, �s az el�z� fokozat kimenete �rv�nyes
    else if(round_ready && normalise_vld) begin
        z_m <= in_z_m[9:0];
        z_e <= in_z_e;
        z_s <= in_z_s;
        round_bit <= in_round_bit;
        guard <= in_guard;
        sticky <= in_sticky;
        round_ready <= 0;   //Dolgozik, nem tud adatot fogadni
        round_ack <= 1;     //Jelez, hogy beolvasta az adatot
    end
    //Ha a k�vetkez� fokozat kiolvasta az adatot
    else if(put_ack) begin
        round_ready <= 1;   //�jra k�sz adatot fogadni
        round_vld   <= 0;   //�rv�nytelen a kimeneten az adat
    end
    //Ha beolvasott egy sz�mot, de a kimenet �rv�nytelen
    else if(!round_ready && !round_vld) begin
        if (guard && (round_bit | sticky | z_m[0])) begin
            z_m <= z_m + 1;
            if (z_m == 10'b1111111111) begin
                z_e <=z_e + 1;
            end
        end
        round_vld <= 1;
        round_ack <= 0;
    end
end
    
endmodule
