`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 07:08:15 PM
// Design Name: 
// Module Name: read
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module read(
    input clk,rst,
    
    input [15:0] in_x,      //Egyik operandus
    input [15:0] in_y,      //M�sik operandus
    
    input in_vld,           //Bemeneten �rv�nyes adat
    input special_ack,      //K�sz a k�l�nleges esetek vizsg�lata
    
    output reg read_ack,      //Bemenet beolvasta az adatot
    output reg read_vld,
    
    output reg [13:0] x_m, y_m, // 1 rejtett bit, 10 sspecial_zignifik�ns, 1 gurad, 1 round, 1 sticky
    output reg [4:0] x_e, y_e,  //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg x_s, y_s         //El�jel bitek

    );
    
    reg read_ready;
    
    //Adat beolvas�sa
    always @ (posedge clk) begin
        //Reset ut�n k�sz adatot fogadni, �s �rv�nytelen adatot tartalmaz
        if(rst) begin
            read_ready <= 1;    //K�sz adatot fogadni
            read_vld <= 0;      //Kimenet �rv�nytelen
            read_ack <= 0;
        end
        //Ha �rv�nyes a bemenet �s k�sspecial_z adatot fogadni
        else if(in_vld && read_ready) begin
             x_m <= {in_x[9:0], 3'd0};   //mantissspecial_za x
             x_e <= in_x[14:10] - 15;    //exponens  x
             x_s <= in_x[15];            //el�jel    x
             y_m <= {in_y[9:0], 3'd0};   //mantissspecial_za y
             y_e <= in_y[14 : 10] - 15;  //exponens  y
             y_s <= in_y[15];            //el�jel    y
             read_ready <= 0;            //Nem tud adatot fogadni
             read_ack <= 1;              //Jelzi, hogy beolvasta az adatot
             read_vld <= 1;              //Jelzi, hogy �rv�nyes adatot tartalmaz
        end
        //Ha a k�vetkez� egys�g kiolvasta az adatot, akkor � �jra fogadhat
        //�s a kimenete �j adat fogad�s�ig �rv�nytelen
        else if(special_ack) begin
            read_ready <= 1;    //�jra k�sz adatot fogadni
            read_vld <= 0;      //�rv�nytelen az adat (elavlult)
        end
        else
            read_ack <= 0;
    end
endmodule
