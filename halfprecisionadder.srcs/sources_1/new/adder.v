`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.04.2018 19:18:07
// Design Name: 
// Module Name: adder_sajat
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder(
    input clk,rst,
    input [15:0] in_x,
    input [15:0] in_y,
    input in_stb,
    
    output [15:0] out_z,
    output stb
    );
    
reg [15:0] s_output_z;
    
reg [3:0] state;

parameter   read            = 4'd0,   //Beolvas�s �s kibont�s
            special_cases   = 4'd3,   //K�l�nleges esetek kezel�se
            align           = 4'd4,   //K�z�s exponensre hoz�s
            add             = 4'd5,   //�sszead�s
            carry           = 4'd6,   //Carry
            normalise       = 4'd7,   //Normaliz�lt alakra hoz�s
            subnormal       = 4'd8,   //Normaliz�lt alakre hoz�s
            round           = 4'd9,   //Kerek�t�s
            pack            = 4'd10,  //Kimenet el��ll�t�sa
            put_z           = 4'd11;

reg [15:0] z;               //�tmeneti regsizter a kimeneti adatnak
reg [13:0] x_m, y_m;        // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
reg [10:0] z_m;             //Eredm�ny mantissz�ja rejtett bittel, de v�de�bitek n�lk�l
reg [4:0] x_e, y_e, z_e;    //A bemeneti operandusok �s az eredm�ny exponens bitjei
reg x_s, y_s, z_s;          //El�jel bitek
reg guard, round_bit, sticky;
reg [14:0] sum;             //K�t mantissza �sszege
reg s_stb;

always @(posedge clk)
begin
    case(state)
    //K�t operandus beolvas�sa �s sz�tbont�sa
    read:
    begin
        if (in_stb) begin
        x_m <= {in_x[9:0], 3'd0};   //mantissza x
        x_e <= in_x[14:10] - 15;    //exponens  x
        x_s <= in_x[15];            //el�jel    x
        y_m <= {in_y[9:0], 3'd0};   //mantissza y
        y_e <= in_y[14 : 10] - 15;  //exponens  y
        y_s <= in_y[15];            //el�jel    y
        state <= special_cases;     //K�vetkez� �llapot: k�l�nleges esetek kezel�se
        end
    end

    //K�l�nleges esetek kezel�se
    special_cases:
    begin
    //Ha valamelyik bemenet egy NaN,
    //vagy ha mindkett� bemenet v�gtlen �s k�l�nb�zik az el�jel�k,
    //akkor a kimenet NaN
    if(((x_e == 16 && x_m[12:0] != 0) || (y_e == 16 && y_m[12:0] != 0)) || (((x_e == 16) &&  (y_e == 16))  && (x_s != y_s))) begin
        z[15] <= 1;
        z[14:10] <= 5'b11111;   //A csupa egyes exponens nem nulla mantissz�val jelzi, hogy a sz�m NaN
        z[9] <= 1;
        z[8:0] <= 0;
        state <= put_z;
    end
    //Abban az esetben, ha mindkett� v�gtelen azonos el�jellel,
    //vagy csak az x v�gtelen, akkor a kimenet v�gtelen
    else if(x_e == 16) begin
        z[15] <= x_s;         //A kimenet el�jele x el�jele (+- v�ggtelen)
        z[14:10] <= 5'b11111; //A csupa egyes exponens nulla mantissz�val jelzi, hogy a sz�m v�gtelen
        z[9:0] <= 0;          //Nulla mantissza
        state <= put_z;       //Nem kell tov�bb sz�molni. Sz�m al�k�sz�t�se kiad�shoz
    end
     //vagy csak az y v�gtelen, akkor a kimenet v�gtelen
    else if(y_e == 16) begin
        z[15] <= y_s;         //A kimenet el�jele y el�jele (+- v�ggtelen)
        z[14:10] <= 5'b11111; //A csupa egyes exponens nulla mantissz�val jelzi, hogy a sz�m v�gtelen
        z[9:0] <= 0;          //Nulla mantissza
        state <= put_z;       //Nem kell tov�bb sz�molni. Sz�m al�k�sz�t�se kiad�shoz
    end
    //Ha a �s b is nulla a kimenet nulla
    else if ((($signed(x_e) == -15) && (x_m[12:0] == 0)) && (($signed(y_e) == -15) && (y_m[12:0] == 0))) begin
      z[15] <= x_s & y_s;           //Ha mindk�t el�jel bit 1, akkor a m�nusz nulla
      z[14:10] <= x_e[4:0] + 15;    
      z[9:0] <= x_m[12:3];
      state <= put_z;
    end
    //Ha x nulla, akkor az eredm�ny y
    else if (($signed(x_e) == -15) && (x_m[12:0] == 0)) begin
      z[15] <= y_s;
      z[14:10] <= y_e[4:0] + 15;    
      z[9:0] <= y_m[12:3];
      state <= put_z;
    end
    //ha y nulla, akkor az eredm�ny x
    else if (($signed(y_e) == -15) && (y_m[12:0] == 0)) begin
      z[15] <= x_s;
      z[14:10] <= x_e[4:0] + 15;
      z[9:0] <= x_m[12:3];
      state <= put_z;
    end
    //Hogyha norm�lis vagy szubnorm�lis a sz�m
    else begin
        //Hogyha x szubnorm�lis a rejtett bit 0 egy�bk�nt 1
        if ($signed(x_e) == -15)
            x_m[13] <= 0;   //A rejtett bit 0
        else
            x_m[13] <= 1;   //A rejtett bit 1
        //Hogyha y szubnorm�lis a rejtett bit 0 egy�bk�nt 1
        if ($signed(y_e) == -15)
            y_m[13] <= 0;   //A rejtett bit 0
        else
            y_m[13] <= 1;   //A rejtett bit 1
        
        //K�vetkez� l�p�s k�z�s exponensre hoz�s
         state <= align;
    end
end

    //K�z�s exponensre hoz�s, az eredm�nyben a nagyobb sz�mnak kell domin�lnia
    align:
    begin
        //Ha az x exponensre a nagyobb, akkor y mantissz�j�t osztjuk 2-vel
        //�s az exponenst n�velj�k eggyel addig, ameddig meg nem egyezik a k�t exponens
        if ($signed(x_e) > $signed(y_e)) begin
            y_e <= y_e + 1;     //Exponens n�vel�se
            y_m <= y_m >> 1;    //Mantissza shiftel�se balra (oszt�s kett�vel)
            y_m[0] <= y_m[0] | y_m[1];
        end
        //Ha az y exponensre a nagyobb, akkor x mantissz�j�t osztjuk 2-vel
        //�s az exponenst n�velj�k eggyel addig, ameddig meg nem egyezik a k�t exponens
        else if ($signed(x_e) < $signed(y_e)) begin
            x_e <= x_e + 1;     //Exponens n�vel�se
            x_m <= x_m >> 1;    //Mantissza shiftel�se balra (oszt�s kett�vel)
            x_m[0] <= x_m[0] | x_m[1];
    end
    else begin
        //K�vetkez� l�p�s az �sszead�s
        state <= add;
    end
end
    //K�t sz�m mantissz�j�nak �sszead�sa
    add:
    begin
        //A kimenet exponensre a k�z�s exponens lesz
        z_e <= x_e;
        //Hogyha a k�t el�jel megegyezik
        if (x_s == y_s) begin
            sum <= x_m + y_m;   //Mantissz�k �sszead�sa
            z_s <= x_s;         //Az el�jel nem v�ltozik
        end
        //Ha a k�t el�jel nem egyezik meg
        //Az abszol�t �rt�kben nagyobb sz�mb�l kivonjuk a kisebbet
        else begin
            //Ha x mantissz�ja abszol�t �rt�kben nagyobb, mint y mantissz�ja
            if (x_m >= y_m) begin
                sum <= x_m - y_m;   //Mantissz�k kivon�sa egym�sb�l
                z_s <= x_s;         //A kimenet el�je az abszol�t �rt�kben nagyobb sz�m el�jele
            end
            //Ha y mantissz�ja abszol�t �rt�kben nagyobb, mint x mantissz�ja
            else begin
                sum <= y_m - x_m;   //Mantissz�k kivon�sa egym�sb�l
                z_s <= y_s;         //A kimenet el�je az abszol�t �rt�kben nagyobb sz�m el�jele
            end
        end
    state <= carry;     //K�vetkez� �llapot az �tvitel kezel�se
    end

    //A keletkezett �tvitel kezel�se
    carry:
    begin
        //Ha az �sszeg MSB-je 1 (rejtett bit carry)
        if (sum[14]) begin
            z_m <= sum[14:4];   //Az eredm�nyt eggyel belra shifttelve �rjuk �t az eredm�ny mantissz�j�ba
            z_e <= z_e + 1;             //A shiftel�s miatt n�velni kell az exponenst
            guard <= sum[3];            //Guard bit bi�ll�t�sa
            round_bit <= sum[2];        //Round bit bi�ll�t�sa
            sticky <= sum[1] | sum[0];  //Stricky bit be�ll�t�sa
        end
        //Ha nincs rejtett bit carry (p�ld�ul denormaliz�lz sz�mokat adtunk �ssze)
        else begin
            z_m <= sum[13:3];       //Ha nem volt �tvitel, akkor az �sszeg m�dos�t�s n�lk�l haszn�lhat�
            guard <= sum[2];        //Guard
            round_bit <= sum[1];    //Round
            sticky <= sum[0];       //Sticky
        end
        
        state <= normalise;     //K�vetkez� l�p�s a normaliz�l�s
    end

    //Sz�m normaliz�l�sa, ha lehets�ges.
    normalise:
    begin
        //Ha a rejtett bit �rt�ke nulla
        //Az eredm�ny addig kell balra shiftelni (szorozni 2-vel), ameddig az MSB 1 nem lesz,
        //�s az exponensb�l a 2-vel val� szorz�s miatt le kell vonni 1-et
        if (z_m[10] == 0 && $signed(z_e) > -14) begin
            z_m <= z_m << 1;    //Mantissza szorz�sa kett�vel
            z_e <= z_e - 1;     //Exponens cs�kkent�se
            z_m[0] <= guard;
            guard <= round_bit;
            round_bit <= 0;
        end
        
        else begin
            state <= subnormal;     //K�vetkez� �llapot a szubnorm�lis �rt�kek kezel�se
        end
    end

    //Szubnorm�lis v�geredm�ny kezel�se
    subnormal:
    begin
        //Ha az exponens kisebb mint 15, akkor az eltol�s miatt olyan alakra kell hozni,
        //hogy az exponens legal�bb 15 legyen, k�l�nben �rtelmetlen az exponens.
        if ($signed(z_e) < -15) begin //TODO: Ezt �t�rtam 15-re 14r�l mi�rt jobb �gy?
            z_e <= z_e + 1;
            z_m <= z_m >> 1;
            guard <= z_m[0];
            round_bit <= guard;
            sticky <= sticky | round_bit;
        end
        else begin
            state <= round;
        end
    end

    round:
    begin
        if (guard && (round_bit | sticky | z_m[0])) begin
            z_m <= z_m + 1;
            if (z_m == 10'b1111111111) begin
                z_e <=z_e + 1;
            end
        end
        state <= pack;
    end

    pack:
    begin
        z[9:0] <= z_m[9:0];
        z[14:10] <= z_e[4:0] + 15;
        z[15] <= z_s;
    //if overflow occurs, return inf
        if ($signed(z_e) > 15) begin
            z[9 : 0] <= 0;
            z[14 : 10] <= 31;
            z[15] <= z_s;
        end
        state <= put_z;
    end

    put_z:
    begin
        s_stb <= 1;
        s_output_z <= z;
        if (s_stb) begin  //K�sleltet�s 1 �rajellel
            state <= read;
            s_stb<=0;
        end
    end

    endcase

    if (rst == 1) begin
        state <= read;
    end

end


assign out_z = s_output_z;
assign stb = s_stb;

endmodule
