`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 08:05:09 PM
// Design Name: 
// Module Name: align
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module align(

    input clk,rst,
    
    input special_vld,              //El�z� fokozat k�sz
    input add_ack,                  //K�vetkez� fokozat elvette
    
    input [13:0] in_x_m, in_y_m,    // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    input [4:0] in_x_e, in_y_e,     //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_x_s, in_y_s,           //El�jel bitek
    
    output reg align_vld,           //Az adat k�sz tov�bbi feldolgoz�sra
    output reg align_ack,           //Jelz�s, hogy adatot olvasott be
    output reg align_ready,
    
    output reg [13:0] x_m, y_m,     // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    output reg [4:0] x_e, y_e,      //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg x_s, y_s             //El�jel bitek

    );
    
    
always @ (posedge clk) begin
    if(rst) begin
        align_ready   <= 1;   //K�sz adatot fogadni
        align_vld     <= 0;   //�rv�nytelen adat
        align_ack     <= 0;
    end            
    //Hogyha tud adatot fogadni, �s a special kimenet�n �rv�nyes
    //adat van, akkor �tveszi az adatot �s nyugt�z
    else if(align_ready && special_vld) begin
        x_m <= in_x_m;  //mantissza x
        x_e <= in_x_e;  //exponens  x
        x_s <= in_x_s;  //el�jel    x
        y_m <= in_y_m;  //mantissza y
        y_e <= in_y_e;  //exponens  y
        y_s <= in_y_s;  //el�jel    y
        align_ready <= 0;         //Dolgozik
        align_ack <= 1;           //Nyugta, hogy beolvasta az adatot
        end
    //Az �sszead� kiolvasta az adatot
    else if(add_ack) begin
        align_ready   <= 1;   //�jra k�sz adatot fogadni
        align_vld     <= 0;   //�rv�nytelen adat
    end
    else if(!align_ready && !align_vld) begin
        //Ha az x exponensre a nagyobb, akkor y mantissz�j�t osztjuk 2-vel
        //�s az exponenst n�velj�k eggyel addig, ameddig meg nem egyezik a k�t exponens
        if ($signed(x_e) > $signed(y_e)) begin
            y_e <= y_e + 1;     //Exponens n�vel�se
            y_m <= y_m >> 1;    //Mantissza shiftel�se balra (oszt�s kett�vel)
            y_m[0] <= y_m[0] | y_m[1];  //Sticky bit �ll�t�sa
        end
        //Ha az y exponensre a nagyobb, akkor x mantissz�j�t osztjuk 2-vel
        //�s az exponenst n�velj�k eggyel addig, ameddig meg nem egyezik a k�t exponens
        else if ($signed(x_e) < $signed(y_e)) begin
            x_e <= x_e + 1;     //Exponens n�vel�se
            x_m <= x_m >> 1;    //Mantissza shiftel�se balra (oszt�s kett�vel)
            x_m[0] <= x_m[0] | x_m[1];
        end
        //Ha a mantissz�k egyenl�ek, akkor �ssze lehet adni
        else if(($signed(x_e) == $signed(y_e)) && !align_ready)
            align_vld <= 1;
        align_ack <= 0;
    end
end
    

endmodule
