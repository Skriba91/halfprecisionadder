`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/15/2018 08:44:27 AM
// Design Name: 
// Module Name: seg7
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seg7(
    input clk,
    input rst,
    
    input [3:0] dig0,
    input [3:0] dig1,
    input [3:0] dig2,
    input [3:0] dig3,
    
    output reg [3:0] disp,
    output reg [7:0] seg
);

reg [16:0] cntr_div;
always @ (posedge clk)
if (rst)
    cntr_div <= 0;
else if (cntr_div[16])
    cntr_div <= 0;
else
    cntr_div <= cntr_div + 1; 

wire ce;
assign ce = cntr_div[16];

reg [3:0] disp_shr;
reg [1:0] disp_cntr;
always @ (posedge clk)
if (rst)
begin
    disp_shr  <= 4'b0001;
    disp_cntr <= 2'b0;
end
else if (ce)
begin
    disp_shr  <= {disp_shr[2:0], disp_shr[3]};
    disp_cntr <= disp_cntr + 1; 
end

reg [3:0] mux;
always @ ( * )
case (disp_cntr)
    2'b00: mux <= dig0;
    2'b01: mux <= dig1;
    2'b10: mux <= dig2;
    2'b11: mux <= dig3;
endcase

// 7-segment encoding
//      0
//     ---
//  5 |   | 1
//     --- <--6
//  4 |   | 2
//     ---
//      3
reg [7:0] seg_val;
always @(mux)
case (mux)
    4'b0001 : seg_val = 8'b11111001;   // 1
    4'b0010 : seg_val = 8'b10100100;   // 2
    4'b0011 : seg_val = 8'b10110000;   // 3
    4'b0100 : seg_val = 8'b10011001;   // 4
    4'b0101 : seg_val = 8'b10010010;   // 5
    4'b0110 : seg_val = 8'b10000010;   // 6
    4'b0111 : seg_val = 8'b11111000;   // 7
    4'b1000 : seg_val = 8'b10000000;   // 8
    4'b1001 : seg_val = 8'b10010000;   // 9
    4'b1010 : seg_val = 8'b10001000;   // A
    4'b1011 : seg_val = 8'b10000011;   // b
    4'b1100 : seg_val = 8'b11000110;   // C
    4'b1101 : seg_val = 8'b10100001;   // d
    4'b1110 : seg_val = 8'b10000110;   // E
    4'b1111 : seg_val = 8'b10001110;   // F
    default  : seg_val = 8'b11000000;   // 0
endcase

always @ (posedge clk)
begin
    disp      <= disp_shr;
    seg[7:0]  <= ~seg_val;
end

endmodule
