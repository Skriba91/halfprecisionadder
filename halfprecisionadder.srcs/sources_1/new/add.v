`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 08:40:27 PM
// Design Name: 
// Module Name: add
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module add(

    input clk,rst,
    
    input align_vld,                //El�z� fokozat k�sz
    input carry_ack,                //K�vetkez� fokozat elvette
    
    input [13:0] in_x_m, in_y_m,    // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    input [4:0] in_x_e, in_y_e,     //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_x_s, in_y_s,           //El�jel bitek
    
    output reg add_vld,             //Az adat k�sz tov�bbi feldolgoz�sra
    output reg add_ack,             //Jelz�s, hogy adatot olvasott be
    output reg add_ready,
    
    output reg [13:0] x_m, y_m,     // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    output reg [4:0] x_e, y_e, z_e, //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg x_s, y_s, z_s,       //El�jel bittek
    
    output reg [14:0] sum           //�sszeg

    );


always @ (posedge clk) begin
    //Reset ut�n az kimen� adat �rv�nytlen, �s k�sz adatot fogadni
    if(rst) begin
        add_vld <= 0;
        add_ready <= 1;
    end
    //Ha k�z�s exponensen vannak beolvassa az adatot
    else if(add_ready && align_vld) begin
        x_m <= in_x_m;  //mantissza x
        x_e <= in_x_e;  //exponens  x
        x_s <= in_x_s;  //el�jel    x
        y_m <= in_y_m;  //mantissza y
        y_e <= in_y_e;  //exponens  y
        y_s <= in_y_s;  //el�jel    y
        add_ready <= 0;         //Dolgozik
        add_ack <= 1;           //Nyugta, hogy beolvasta az adatot
    end
    //Miut�n a carry blokk beolvasta az adatot
    else if(carry_ack) begin
        add_vld <= 0;   //Kimeneti adat �rv�nytelen
        add_ready <= 1; //�jra k�sz adatot fogadni
    end
    //Ha van adat, de a kimenet m�g nem �rv�nyes
    else if(!add_ready && !add_vld) begin
        add_ack <= 0;
        z_e <= x_e;
        //Hogyha a k�t el�jel megegyezik
        if (x_s == y_s) begin
            sum <= x_m + y_m;   //Mantissz�k �sszead�sa
            z_s <= x_s;         //Az el�jel nem v�ltozik
        end
        //Ha a k�t el�jel nem egyezik meg
        //Az abszol�t �rt�kben nagyobb sz�mb�l kivonjuk a kisebbet
        else begin
            //Ha x mantissz�ja abszol�t �rt�kben nagyobb, mint y mantissz�ja
            if (x_m >= y_m) begin
                sum <= x_m - y_m;   //Mantissz�k kivon�sa egym�sb�l
                z_s <= x_s;         //A kimenet el�je az abszol�t �rt�kben nagyobb sz�m el�jele
            end
            //Ha y mantissz�ja abszol�t �rt�kben nagyobb, mint x mantissz�ja
            else begin
                sum <= y_m - x_m;   //Mantissz�k kivon�sa egym�sb�l
                z_s <= y_s;         //A kimenet el�je az abszol�t �rt�kben nagyobb sz�m el�jele
            end
        end
        add_vld <= 1;   //A kimenet �rv�nyes
    end
end
endmodule
