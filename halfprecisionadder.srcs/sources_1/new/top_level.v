module top_level(
input clk,
input rst,
input ready_btn,
output [3:0] disp,
output [7:0] seg,
output [3:0] led
);

wire [15:0]romx;
wire [15:0]romy;
reg [3:0] address;
lut_rom lut_rom0(
   .address(address),
   .data(romx),
   .data2(romy)
);

//reg in_stb;
wire [15:0]adderx;
wire [15:0]addery;
wire [15:0]adderz;
//wire stb;
/*adder adder_0(
    .clk(clk),
    .rst(rst),
    .in_x(adderx),
    .in_y(addery),
    .in_stb(in_stb),  
    .out_z(adderz),
    .stb(stb)
);*/


wire in_vld;       //Bementi adat �rv�nyes
wire read_ack;     //Az �sszead� elvette a bemeneti adatot
wire out_vld;      //Kimeneti adat �rv�nyes
wire out_ack;      //Kiolvast�k a kimeneti adatot

//�sszead� egys�g
adder_pipe adder_pipe_0(
    .clk(clk),
    .rst(rst),              //�rajel �s reset
    .in_x(adderx),          //Egyik operandus
    .in_y(addery),          //M�sik operandus
    .in_vld(in_vld),        //Bemeneten �rv�nyes adat
    .out_ack(out_ack),      //Elvett�k a kimeneti adatot
    .read_ack(read_ack),    //Bemenet beolvasta az adatot
    .out_vld(out_vld),      //Kimeneten �rv�nyes adat
    .out_z(adderz)          //Kimeneti adat
    );


wire [3:0]first;
wire [3:0]second;
wire [3:0]third;
wire [3:0]fourth;

//7 szegmens kijelz�
seg7 seg7_0(
    .clk(clk),
    .rst(rst),
    .dig0(first),
    .dig1(second),
    .dig2(third),
    .dig3(fourth),
    .disp(disp),
    .seg(seg)
);

reg [15:0]z;    //Regiszter az �sszead� eredm�ny�nek t�rol�s�ra, innen olvas a h�tszegmens
reg out_ack_ff; //Jelz�s, hogy beolvastuk az �sszead� kimenet�n l�v� adatot

//Adat elv�tele a h�tszegmens kijelz� sz�m�ra
always @ (posedge clk)
    //Ha a kimeneti adat �rv�nyes
    if(out_vld) begin
        z<=adderz;          //Adat kiolvas�sa
        out_ack_ff <= 1;    //Jelez�s, hogy az adat ki van olvasva
    end
    else
        out_ack_ff <= 0;

assign out_ack = out_ack_ff;

assign first=z[3:0];
assign second=z[7:4];
assign third=z[11:8];
assign fourth=z[15:12];

//Felfut� �l detekt�l�s�hoz 3 ff 
reg [2:0] ready;
always @ (posedge clk)
    ready <= {ready[1:0], ready_btn};

//Felfut� �l detekt�l�s    
wire btn_rise;
//assign btn_rise = 1;  //Tesztel�hez
assign btn_rise = (!ready[2] && ready[1]);  //Bemutat�shoz

reg in_vld_ff;  //Bemeneti �rv�nyes jel

reg [3:0] address_led_ff;

always @ (posedge clk)
    //Reset eset�n a bemenet �rv�nytelen, kezd�c�m 0
    if(rst) begin
        in_vld_ff <= 0;
        address <= 0;
    end
    //Az �sszead� v�gzett az �sszead�ssal
    else if(read_ack) begin
        in_vld_ff <= 0;            //A bemeneti c�m �rv�nytelen
        address <= address + 1; //C�m l�ptet�se
        address_led_ff <= address;
    end
    else if(btn_rise)
        in_vld_ff <= 1;            //Bemeneti c�m �jra �rv�nyes

assign in_vld = in_vld_ff;
assign led = address_led_ff;

assign adderx=romx;
assign addery=romy;

endmodule