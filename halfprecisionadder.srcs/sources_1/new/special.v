`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 07:18:18 PM
// Design Name: 
// Module Name: special
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module special(
    input clk,rst,
    
    input read_vld,                 //El�z� szint �rv�nyes
    input align_ack,                //K�vetkez� szint beolvasta
    input put_special_ack,          //Legutols� szint beolvasta
    
    input [13:0] in_x_m, in_y_m,   // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    input [4:0] in_x_e, in_y_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_x_s, in_y_s,          //El�jel bitek

    output reg special_out_vld,     //Az adat k�sz kiad�sra
    output reg special_vld,         //Az adat k�sz tov�bbi feldolgoz�sra
    output reg special_ack,         //Jelz�s, hogy adatot olvasott be
    
    output reg [13:0] x_m, y_m,     //1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    output reg [4:0] x_e, y_e,      //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg x_s, y_s,            //El�jel bitek
    output reg [15:0] z

    );

//Az egys�g �llapot�nak t�rol�sa    
reg special_ready;
    
    //K�l�nleges esetek kezel�se
    always @ (posedge clk) begin
        if(rst) begin
            special_ready   <= 1;   //K�sz adatot fogadni
            special_vld     <= 0;   //�rv�nytelen adat
            special_out_vld <= 0;   //�rv�nytelen adat
            special_ack     <= 0;   //Handshake
        end            
        //Hogyha tud adatot fogadni, �s a beolvas� kimenet�n �rv�nyes
        //adat van, akkor �tveszi az adatot �s nyugt�z
        else if(special_ready && read_vld) begin
            x_m <= in_x_m;  //mantissza x
            x_e <= in_x_e;  //exponens  x
            x_s <= in_x_s;  //el�jel    x
            y_m <= in_y_m;  //mantissza y
            y_e <= in_y_e;  //exponens  y
            y_s <= in_y_s;  //el�jel    y
            special_ready <= 0;         //Dolgozik
            special_ack <= 1;           //Nyugta, hogy beolvasta az adatot
        end
        //Ha a k�vetkez� egys�g kiolvasta az adatot, akkor � �jra fogadhat
        //�s a kimenete �j adat fogad�s�ig �rv�nytelen
        else if(align_ack | put_special_ack) begin
            special_ready   <= 1;   //�jra fogad
            special_vld     <= 0;   //�rv�nytelen adat
            special_out_vld <= 0;   //�rv�nytelen adat
        end
        //Van bent �rv�nyes adat �s egyik kimenete sem valid
        else if(!special_ready && !special_out_vld && !special_vld) begin
            //Ha valamelyik bemenet egy NaN,
            //vagy ha mindkett� bemenet v�gtlen �s k�l�nb�zik az el�jel�k,
            //akkor a kimenet NaN
            if(((x_e == 16 && x_m[12:0] != 0) || (y_e == 16 && y_m[12:0] != 0)) || (((x_e == 16) &&  (y_e == 16))  && (x_s != y_s))) begin
                z[15] <= 1;
                z[14:10] <= 5'b11111;   //A csupa egyes exponens nem nulla mantissz�val jelzi, hogy a sz�m NaN
                z[9] <= 1;
                z[8:0] <= 0;
                special_out_vld <= 1;   //Az nincs t�bb munka az adattal, mehet a kimentre
            end
             //Abban az esetben, ha mindkett� v�gtelen azonos el�jellel,
             //vagy csak az x v�gtelen, akkor a kimenet v�gtelen
            else if(x_e == 16) begin
                z[15] <= x_s;         //A kimenet el�jele x el�jele (+- v�ggtelen)
                z[14:10] <= 5'b11111; //A csupa egyes exponens nulla mantissz�val jelzi, hogy a sz�m v�gtelen
                z[9:0] <= 0;          //Nulla mantissza
                special_out_vld <= 1; //Az nincs t�bb munka az adattal, mehet a kimentre
            end
            //vagy csak az y v�gtelen, akkor a kimenet v�gtelen
            else if(y_e == 16) begin
                z[15] <= y_s;         //A kimenet el�jele y el�jele (+- v�ggtelen)
                z[14:10] <= 5'b11111; //A csupa egyes exponens nulla mantissz�val jelzi, hogy a sz�m v�gtelen
                z[9:0] <= 0;          //Nulla mantissza
                special_out_vld <= 1; //Az nincs t�bb munka az adattal, mehet a kimentre
            end
            //Ha a �s b is nulla a kimenet nulla
            else if ((($signed(x_e) == -15) && (x_m[12:0] == 0)) && (($signed(y_e) == -15) && (y_m[12:0] == 0))) begin
                z[15] <= x_s & y_s;         //Ha mindk�t el�jel bit 1, akkor a m�nusz nulla
                z[14:10] <= x_e[4:0] + 15;    
                z[9:0] <= x_m[12:3];
                special_out_vld <= 1;   //Az nincs t�bb munka az adattal, mehet a kimentre
            end
            //Ha x nulla, akkor az eredm�ny y
            else if (($signed(x_e) == -15) && (x_m[12:0] == 0)) begin
                z[15] <= y_s;
                z[14:10] <= y_e[4:0] + 15;    
                z[9:0] <= y_m[12:3];
                special_out_vld <= 1;   //Az nincs t�bb munka az adattal, mehet a kimentre
            end
            //ha y nulla, akkor az eredm�ny x
            else if (($signed(y_e) == -15) && (y_m[12:0] == 0)) begin
                z[15] <= x_s;
                z[14:10] <= x_e[4:0] + 15;
                z[9:0] <= x_m[12:3];
                special_out_vld <= 1;
            end
            //Hogyha norm�lis vagy szubnorm�lis a sz�m
            else begin
            //Hogyha x szubnorm�lis a rejtett bit 0 egy�bk�nt 1
                if ($signed(x_e) == -15)
                    x_m[13] <= 0;   //A rejtett bit 0
                else
                    x_m[13] <= 1;   //A rejtett bit 1
                    //Hogyha y szubnorm�lis a rejtett bit 0 egy�bk�nt 1
                if ($signed(y_e) == -15)
                    y_m[13] <= 0;   //A rejtett bit 0
                else
                    y_m[13] <= 1;   //A rejtett bit 1
                special_vld <= 1;   //Az nincs t�bb munka az adattal, mehet a kimentre
            end
            special_ack <= 0;
        end
    end
    
    
endmodule
