`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/30/2018 01:19:40 PM
// Design Name: 
// Module Name: lut_rom
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/30/2018 11:22:26 AM
// Design Name: 
// Module Name: lut_ram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lut_rom(
   input [3:0] address,
   output reg[15:0] data,
   output reg[15:0] data2
    );
    
    
    always@(*)
    case(address)
        4'b0000: data<=16'b0100000100000000;   //2.5
        4'b0001: data<=16'b0100101000111010;   //12.45
        4'b0010: data<=16'b0111111000000000;   //NaN
        4'b0011: data<=16'b0100111001010101;   //25.33
        4'b0100: data<=16'b0000000001010100;   //0.000005
        4'b0101: data<=16'b0000000011001001;   //0.000012
        4'b0110: data<=16'b1000101110101111;   //-0.0002345
        4'b0111: data<=16'b0111110000000000;   //inf
        4'b1000: data<=16'b0000000001110101;   //0.000007
        4'b1001: data<=16'b0000000000101110;   //0.0000014
        4'b1010: data<=16'b1110110011100010;   //-5000
        4'b1011: data<=16'b1111110000000000;   //-inf
        4'b1100: data<=16'b0101100111000000;   //184
        4'b1101: data<=16'b1100010000001110;   //-4.055
        4'b1110: data<=16'b0101011001010000;   //101
        4'b1111: data<=16'b1100010111100000;   //-5.875
    endcase

    always@(*)
        case(address)
            4'b0000: data2<=16'b0011110111110101;   //1.489
            4'b0001: data2<=16'b0100000000111111;   //2.123
            4'b0010: data2<=16'b0101001011000111;   //54.22
            4'b0011: data2<=16'b0011001000010100;   //0.19
            4'b0100: data2<=16'b0011110000000000;   //1
            4'b0101: data2<=16'b0000000010010111;   //0.000009
            4'b0110: data2<=16'b0000010100011101;   //0.000078
            4'b0111: data2<=16'b1111110000000000;   //-inf
            4'b1000: data2<=16'b1000000010000110;   //-0.000008
            4'b1001: data2<=16'b1000010110110111;   //-0.0000872
            4'b1010: data2<=16'b0111110000000000;   //inf
            4'b1011: data2<=16'b0110111011010110;   //7000
            4'b1100: data2<=16'b0011100111111100;   //0.748
            4'b1101: data2<=16'b0101110101010100;   //341
            4'b1110: data2<=16'b1010100111010001;   //-0.04543
            4'b1111: data2<=16'b0100011010001011;   //6.543 
        endcase
    
endmodule

