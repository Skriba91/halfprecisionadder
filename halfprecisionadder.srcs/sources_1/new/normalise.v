`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/19/2018 09:54:52 AM
// Design Name: 
// Module Name: normalise
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module normalise(

    input clk,rst,
    
    input carry_vld,
    input round_ack,
    
    input [10:0] in_z_m,
    input [4:0] in_z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_z_s,          //El�jel bitek
    input in_round_bit, in_guard, in_sticky,
    
    output reg normalise_vld,         //Az adat k�sz tov�bbi feldolgoz�sra
    output reg normalise_ack,         //Jelz�s, hogy adatot olvasott be
    output reg normalise_ready,       //K�sz adatot fogadni
    
    output reg [10:0] z_m,   //Az eredm�ny mantissz�ja rejtett bittel
    output reg [4:0] z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg z_s,          //El�jel bit
    output reg round_bit, guard, sticky //Szabv�ny �ltal defini�lt seg�d bitek


    );
    
always @ (posedge clk) begin
    //Reset ut�n k�sz adatot fogadni, �s a kimenete �rv�nytelen
    if(rst)begin
        normalise_ready <= 1;
        normalise_vld <= 0;
        normalise_ack <= 0;
    end
    //Ha k�sz adatot fogadni, �s az el�z� szint kimenet�n �rv�nyes az adat
    else if(normalise_ready && carry_vld) begin
        //Adatbeolvas�s
        z_m <= in_z_m;
        z_e <= in_z_e;
        z_s <= in_z_s;
        round_bit <= in_round_bit;
        guard <= in_guard;
        sticky <= in_sticky;
        normalise_ready <= 0;   //Dolgozik, nem tud adatot fogadni
        normalise_ack <= 1;     //Jelez, hogy beolvasta az adatot
    end
    //Ha a k�vetkez� szint elvette az adatot
    else if(round_ack) begin
        normalise_ready <= 1;   //�jra k�sz adatot fogadni
        normalise_vld <= 0;     //�rv�nytelen a kimenet
    end
    //Ha a rejtett bit �rt�ke nulla
    //Az eredm�ny addig kell balra shiftelni (szorozni 2-vel), ameddig az MSB 1 nem lesz,
    //�s az exponensb�l a 2-vel val� szorz�s miatt le kell vonni 1-et
    else if(!normalise_ready && !normalise_vld) begin
        if (z_m[10] == 0 && $signed(z_e) > -14) begin
            z_m <= z_m << 1;    //Mantissza szorz�sa kett�vel
            z_e <= z_e - 1;     //Exponens cs�kkent�se
            z_m[0] <= guard;
            guard <= round_bit;
            round_bit <= 0;
        end
        //Ha az exponens kisebb mint 15, akkor az eltol�s miatt olyan alakra kell hozni,
        //hogy az exponens legal�bb 15 legyen, k�l�nben �rtelmetlen az exponens.
        else if ($signed(z_e) < -15) begin
            z_e <= z_e + 1;
            z_m <= z_m >> 1;
            guard <= z_m[0];
            round_bit <= guard;
            sticky <= sticky | round_bit;
        end
        else
            normalise_vld <= 1; //Ha nincs munka az adattal �rv�nyes a kimenet
        normalise_ack <= 0;
    end
end    

endmodule
