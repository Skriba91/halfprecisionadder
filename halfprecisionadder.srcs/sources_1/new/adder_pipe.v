`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 06:15:03 PM
// Design Name: 
// Module Name: adder_pipe
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_pipe(
    input clk,rst,          //�rajel �s reset
    
    input [15:0] in_x,      //Egyik operandus
    input [15:0] in_y,      //M�sik operandus
    
    input in_vld,           //Bemeneten �rv�nyes adat
    input out_ack,          //Elvett�k a kimeneti adatot
    
    output read_ack,      //Bemenet beolvasta az adatot
    output out_vld,     //Kimeneten �rv�nyes adat
   
    output [15:0] out_z    //Kimeneti adat
    );

wire special_ack;
wire read_vld;

wire [13:0] read_x_m, read_y_m;
wire [4:0] read_x_e, read_y_e;
wire read_x_s, read_y_s;

read read_0(
    .clk(clk),
    .rst(rst),
    .in_x(in_x),      //Egyik operandus
    .in_y(in_y),      //M�sik operandus
    .in_vld(in_vld),           //Bemeneten �rv�nyes adat
    .special_ack(special_ack),   //k�sz a k�l�nleges esetek vizsg�lata
    .read_ack(read_ack),      //Bemenet beolvasta az adatot
    .read_vld(read_vld),
    .x_m(read_x_m),
    .y_m(read_y_m),    // 1 rejtett bit, 10 sspecial_zignifik�ns, 1 gurad, 1 round, 1 sticky
    .x_e(read_x_e),
    .y_e(read_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .x_s(read_x_s),
    .y_s(read_y_s)          //El�jel bitek
    );
    
wire align_ack;
wire put_ack;
wire put_special_ack;
wire special_vld;
wire special_out_vld;

wire [13:0] special_x_m, special_y_m;
wire [4:0] special_x_e, special_y_e;
wire special_x_s, special_y_s;
wire [15:0] special_z;

special apecial_0(
    .clk(clk),
    .rst(rst),
    .read_vld(read_vld),
    .align_ack(align_ack),
    .put_special_ack(put_special_ack),
    .in_x_m(read_x_m),
    .in_y_m(read_y_m),    // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    .in_x_e(read_x_e),
    .in_y_e(read_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .in_x_s(read_x_s),
    .in_y_s(read_y_s),          //El�jel bitek
    .special_out_vld(special_out_vld),     //Az adat k�sz kiad�sra
    .special_vld(special_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .special_ack(special_ack),         //Jelz�s, hogy adatot olvasott be
    .x_m(special_x_m),
    .y_m(special_y_m),    // 1 rejtett bit, 10 sspecial_zignifik�ns, 1 gurad, 1 round, 1 sticky
    .x_e(special_x_e),
    .y_e(special_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .x_s(special_x_s),
    .y_s(special_y_s),          //El�jel bitek
    .z(special_z)
    );

wire add_ack;
wire align_vld;
wire align_ready;

wire [13:0] align_x_m, align_y_m;
wire [4:0] align_x_e, align_y_e;
wire align_x_s, align_y_s;

align align_0(
    .clk(clk),
    .rst(rst),
    .special_vld(special_vld),
    .add_ack(add_ack),
    .in_x_m(special_x_m),
    .in_y_m(special_y_m),    // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    .in_x_e(special_x_e),
    .in_y_e(special_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .in_x_s(special_x_s),
    .in_y_s(special_y_s),          //El�jel bitek
    .align_vld(align_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .align_ack(align_ack),
    .align_ready(align_ready),
    .x_m(align_x_m),
    .y_m(align_y_m),    // 1 rejtett bit, 10 sspecial_zignifik�ns, 1 gurad, 1 round, 1 sticky
    .x_e(align_x_e),
    .y_e(align_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .x_s(align_x_s),
    .y_s(align_y_s)          //El�jel bitek
    );

wire carry_ack;
wire add_vld;
wire add_ready;

wire [13:0] add_x_m, add_y_m;
wire [4:0] add_x_e, add_y_e, add_z_e;
wire add_x_s, add_y_s, add_z_s;
wire [14:0] add_sum;
  
add add_0(
    .clk(clk),
    .rst(rst),
    .align_vld(align_vld),
    .carry_ack(carry_ack),
    .in_x_m(align_x_m),
    .in_y_m(align_y_m),    // 1 rejtett bit, 10 szignifik�ns, 1 gurad, 1 round, 1 sticky
    .in_x_e(align_x_e),
    .in_y_e(align_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .in_x_s(align_x_s),
    .in_y_s(align_y_s),          //El�jel bitek
    .add_vld(add_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .add_ack(add_ack),
    .add_ready(add_ready),
    .x_m(add_x_m),
    .y_m(add_y_m),    // 1 rejtett bit, 10 sspecial_zignifik�ns, 1 gurad, 1 round, 1 sticky
    .x_e(add_x_e),
    .y_e(add_y_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .z_e(add_z_e),
    .x_s(add_x_s),
    .y_s(add_y_s),          //El�jel bitek
    .z_s(add_z_s),
    .sum(add_sum)
);

wire normalise_ack;
wire carry_vld;
wire carry_ready;

wire [4:0] carry_z_e;
wire carry_z_s;
wire [10:0] carry_z_m;
wire [14:0] carry_sum;
wire carry_round_bit;
wire carry_guard;
wire carry_sticky;

carry carry_0(
    .clk(clk),
    .rst(rst),
    .add_vld(add_vld),
    .normalise_ack(normalise_ack),
    .in_z_e(add_z_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .in_z_s(add_z_s),          //El�jel bitek
    .in_sum(add_sum),
    .carry_vld(carry_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .carry_ack(carry_ack),         //Jelz�s, hogy adatot olvasott be
    .carry_ready(carry_ready),
    .z_e(carry_z_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .z_s(carry_z_s),          //El�jel bittel
    .z_m(carry_z_m),       //Az eredm�ny mantissz�ja rejtett bittel
    .round_bit(carry_round_bit),
    .guard(carry_guard),
    .sticky(carry_sticky)
);

wire round_ack;
wire normalise_vld;
wire normalise_ready;


wire [4:0] normalise_z_e;
wire normalise_z_s;
wire [10:0] normalise_z_m;
wire normalise_round_bit;
wire normalise_guard;
wire normalise_sticky;

normalise normalise_0(
    .clk(clk),
    .rst(rst),
    .carry_vld(carry_vld),
    .round_ack(round_ack),
    .in_z_m(carry_z_m),
    .in_z_e(carry_z_e),
    .in_z_s(carry_z_s),          //El�jel bitek
    .in_round_bit(carry_round_bit),
    .in_guard(carry_guard),
    .in_sticky(carry_sticky),
    .normalise_vld(normalise_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .normalise_ack(normalise_ack),         //Jelz�s, hogy adatot olvasott be
    .normalise_ready(normalise_ready),
    .z_e(normalise_z_e),
    .z_s(normalise_z_s),          //El�jel bittel
    .z_m(normalise_z_m),       //Az eredm�ny mantissz�ja rejtett bittel
    .round_bit(normalise_round_bit),
    .guard(normalise_guard),
    .sticky(normalise_sticky)
);

wire round_vld;
wire round_ready;


wire [4:0] round_z_e;
wire round_z_s;
wire [9:0] round_z_m;
wire round_round_bit;
wire round_guard;
wire round_sticky;

round round_0(
    .clk(clk),
    .rst(rst),
    .normalise_vld(normalise_vld),
    .put_ack(put_ack),
    .in_z_m(normalise_z_m),
    .in_z_e(normalise_z_e),
    .in_z_s(normalise_z_s),
    .in_round_bit(normalise_round_bit),
    .in_guard(normalise_guard), 
    .in_sticky(normalise_sticky),
    .round_vld(round_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .round_ack(round_ack),         //Jelz�s, hogy adatot olvasott be
    .round_ready(round_ready),
    .z_e(round_z_e),
    .z_s(round_z_s),          //El�jel bittel
    .z_m(round_z_m)       //Az eredm�ny mantissz�ja rejtett bittel
);


wire [15:0] put_z;

put_z put_z_0(
    .clk(clk),
    .rst(rst),    
    .align_ready(align_ready),
    .add_ready(add_ready),
    .carry_ready(carry_ready),
    .normalise_ready(normalise_ready),
    .round_ready(round_ready),
    .round_vld(round_vld),
    .special_out_vld(special_out_vld),
    .special_vld(special_vld),
    .out_ack(out_ack),
    .in_z_m(round_z_m),
    .in_z_e(round_z_e),    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    .in_z_s(round_z_s),          //El�jel bitek
    .in_z(special_z),
    .put_vld(out_vld),         //Az adat k�sz tov�bbi feldolgoz�sra
    .put_ack(put_ack),         //Jelz�s, hogy adatot olvasott be 
    .put_special_ack(put_special_ack),
    .z(out_z)       //Az eredm�ny mantissz�ja rejtett bittel
);

endmodule
