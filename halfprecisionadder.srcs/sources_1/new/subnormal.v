`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/19/2018 10:09:12 AM
// Design Name: 
// Module Name: subnormal
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module subnormal(

    input clk,rst,
    
    input normalise_vld,
    input round_ack,
    
    input [10:0] in_z_m,
    input [4:0] in_z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_z_s,          //El�jel bitek
    input in_round_bit, in_guard, in_sticky,
    
    output reg subnormal_vld,         //Az adat k�sz tov�bbi feldolgoz�sra
    output reg subnormal_ack,         //Jelz�s, hogy adatot olvasott be
    output reg subnormal_ready,       //K�sz adatot fogadni
    
    output reg [10:0] z_m,       //Az eredm�ny mantissz�ja rejtett bittel
    output reg [4:0] z_e,    //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg z_s,          //El�jel bittel
    output reg round_bit, guard, sticky

    );
    
always @ (posedge clk) begin
    //Reszet �t�n k�sz adatot fogadni, �s a kimenet �rv�nytelen
    if(rst) begin
        subnormal_ready <= 1;
        subnormal_vld <= 0;
        subnormal_ack <= 0;
    end
    //Ha k�sz adatot fogadni, �s az el�z� szint kimenet�n �rv�nyes a kimenet,
    //akkor bem�solja az adatot
    else if(subnormal_ready && normalise_vld) begin
        z_m <= in_z_m;
        z_e <= in_z_e;
        z_s <= in_z_s;
        round_bit <= in_round_bit;
        guard <= in_guard;
        sticky <= in_sticky;
        subnormal_ready <= 0;   //Dolgozik, nem tud adatot fogadni
        subnormal_ack <= 1;     //Jelez, hogy beolvasta az adatot
    end
    //Ha k�vetkez� fokozat kiolvasta az adatot
    else if(round_ack) begin
        subnormal_ready <= 1;   //�jra k�sz adatot fogadni
        subnormal_vld   <= 0;   //Kimeneti adat �rv�nytelen
    end
    //Hogyha van beolvasott adat, de m�g nem dolgozta fel 
    else if(!subnormal_ready && !subnormal_vld) begin
        //Ha az exponens kisebb mint 15, akkor az eltol�s miatt olyan alakra kell hozni,
        //hogy az exponens legal�bb 15 legyen, k�l�nben �rtelmetlen az exponens.
        if ($signed(z_e) < -15) begin //TODO: Ezt �t�rtam 15-re 14r�l mi�rt jobb �gy?
            z_e <= z_e + 1;
            z_m <= z_m >> 1;
            guard <= z_m[0];
            round_bit <= guard;
            sticky <= sticky | round_bit;
        end
        //A kimenet el�tt
        else if (guard && (round_bit | sticky | z_m[0])) begin
            z_m <= z_m + 1;
            if (z_m == 10'b1111111111) begin
                z_e <=z_e + 1;
            end
        end
        else
            subnormal_vld <= 1; //Ha nincs munka az adattal �rv�nyes a kimenet
        subnormal_ack <= 0;
    end
end
endmodule
