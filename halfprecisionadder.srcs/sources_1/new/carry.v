`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/18/2018 09:54:02 PM
// Design Name: 
// Module Name: carry
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module carry(

    input clk,rst,
    
    input add_vld,
    input normalise_ack,
    
    input [4:0] in_z_e,          //A bemeneti operandusok �s az eredm�ny exponens bitjei
    input in_z_s,                //El�jel bitek
    input [14:0] in_sum,
    
    output reg carry_vld,        //Az adat k�sz tov�bbi feldolgoz�sra
    output reg carry_ack,        //Jelz�s, hogy adatot olvasott be
    output reg carry_ready,
    
    output reg [4:0] z_e,        //A bemeneti operandusok �s az eredm�ny exponens bitjei
    output reg z_s,              //El�jel bittel
    output reg [10:0] z_m,       //Az eredm�ny mantissz�ja rejtett bittel
    output reg round_bit, guard, sticky //Szabv�ny �ltal defini�lt seg�d bitek
    );
    
reg [14:0] sum;

always @ (posedge clk) begin
    //Reset ut�n
    if(rst) begin
        carry_ready <= 1;   //K�sz adatot fogadni
        carry_vld <= 0;     //Kiementen �rv�nytelen adat
        carry_ack <= 0;
    end
    //Ha k�sz adatot fogadni, �s az el�z� szint kimenete �rv�nyes
    else if(carry_ready && add_vld) begin
        //Adatbeolvas�s
        z_e <= in_z_e;
        z_s <= in_z_s;
        sum <= in_sum;
        carry_ready <= 0;         //Dolgozik
        carry_ack <= 1;           //Nyugta, hogy beolvasta az adatot
    end
    //Ha k�vetkez� szint elvette az adatot
    else if(normalise_ack) begin
        carry_ready <= 1;   //�jra k�pes adatot fogadni
        carry_vld <= 0;     //Kimeneten �rv�nytelen adat
    end
    //Ha az �sszeg MSB-je 1 (rejtett bit carry)
    else if(!carry_ready && !carry_vld) begin
        if (sum[14]) begin
            z_m <= sum[14:4];           //Az eredm�nyt eggyel belra shifttelve �rjuk �t az eredm�ny mantissz�j�ba
            z_e <= z_e + 1;             //A shiftel�s miatt n�velni kell az exponenst
            guard <= sum[3];            //Guard bit bi�ll�t�sa
            round_bit <= sum[2];        //Round bit bi�ll�t�sa
            sticky <= sum[1] | sum[0];  //Stricky bit be�ll�t�sa
        end
        //Ha nincs rejtett bit carry (p�ld�ul denormaliz�lz sz�mokat adtunk �ssze)
        else begin
            z_m <= sum[13:3];       //Ha nem volt �tvitel, akkor az �sszeg m�dos�t�s n�lk�l haszn�lhat�
            guard <= sum[2];        //Guard
            round_bit <= sum[1];    //Round
            sticky <= sum[0];       //Sticky    
        end
        carry_vld <= 1;
        carry_ack <= 0;
    end
end

endmodule
