`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.05.2018 13:41:34
// Design Name: 
// Module Name: adder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_tb();
    
reg clk;
reg rst;
reg [15:0] x [4:0];
reg [15:0] y [4:0];
reg [4:0] addr;

//Inputs
reg in_stb;

//outputs
wire [15:0] out_z;
wire stb;

adder add(
    .clk(clk),
    .rst(rst),
    .in_x(x[addr]),
    .in_y(y[addr]),
    .in_stb(in_stb),

    .out_z(out_z),
    .stb(stb)
);

initial begin
    rst <= 1;
    clk <= 0;
    x[0] <= 16'b0100000100000000;   //2.5
    x[1] <= 16'b0100101000111010;   //12.45
    x[2] <= 16'b0111111000000000;   //NaN
    x[3] <= 16'b0100111001010101;   //25.33
    x[4] <= 16'b1110010000000011;
    y[0] <= 16'b0011110111110101;   //1.489
    y[1] <= 16'b0100000000111111;   //2.123
    y[2] <= 16'b0101001011000111;   //54.22
    y[3] <= 16'b0011001000010100;   //0.19
    y[4] <= 16'b0011111100000111;
    #20
    rst <= 0;
end

always #5
    clk <= ~clk;

always @ (posedge clk)
    if(rst) begin
        in_stb <= 0;
        addr <= 0;
    end
    else if(stb) begin
        in_stb <= 0;
        addr <= addr + 1;
     end
    else
        in_stb <= 1;
   

endmodule
