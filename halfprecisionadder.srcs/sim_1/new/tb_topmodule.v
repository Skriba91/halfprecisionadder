`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/01/2018 02:49:29 PM
// Design Name: 
// Module Name: tb_topmodule
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_topmodule();

reg clk=1;
always #5 clk<=~clk;

reg btn = 0;

always #50 btn <= ~btn;

reg rst=1;
initial begin 
#50 rst = 0;
/*#100 btn = 0;
#300000000 btn = 1;
#100 btn = 0;*/
end

wire [3:0] w_disp;
wire [7:0] w_seg;
wire [4:0] led;

top_level top_level_0(
.clk(clk),
.rst(rst),
.disp(w_disp),
.seg(w_seg),
.ready_btn(btn),
.led(led)
);



endmodule
