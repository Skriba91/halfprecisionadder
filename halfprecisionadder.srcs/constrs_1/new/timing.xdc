create_clock -period 10.000 -name clk -waveform {0.000 5.000} [get_ports -filter { NAME =~  "*clk*" && DIRECTION == "IN" }]

set_property PACKAGE_PIN A10 [get_ports ready_btn]
set_property IOSTANDARD LVCMOS33 [get_ports ready_btn]

set_property PACKAGE_PIN E16 [get_ports {led[0]}]
set_property PACKAGE_PIN E17 [get_ports {led[1]}]
set_property PACKAGE_PIN F19 [get_ports {led[2]}]
set_property PACKAGE_PIN C14 [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[0]}]
